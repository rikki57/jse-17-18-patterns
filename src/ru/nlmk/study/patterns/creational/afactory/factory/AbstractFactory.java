package ru.nlmk.study.patterns.creational.afactory.factory;

import ru.nlmk.study.patterns.creational.afactory.bike.Bike;
import ru.nlmk.study.patterns.creational.afactory.car.Car;

public abstract class AbstractFactory {
    public abstract Car createCar();
    public abstract Bike createBike();
}
