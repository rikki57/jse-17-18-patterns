package ru.nlmk.study.patterns.creational.factory;

public class Test {
    public static void main(String[] args) {
        PizzaFactory pizzaFactory = new PizzaFactory();
        Pizza pizza1 = pizzaFactory.createPizza("pepperoni");
        System.out.println(pizza1);
        Pizza pizza2 = pizzaFactory.createPizza("margarita");
        System.out.println(pizza2);
    }
}
