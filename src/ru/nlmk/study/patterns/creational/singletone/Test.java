package ru.nlmk.study.patterns.creational.singletone;

public class Test {
    public static void main(String[] args) {
        Singletone singletone = Singletone.getInstance();
        Singletone singletone1 = Singletone.getInstance();
        singletone.doWork();
    }
}
