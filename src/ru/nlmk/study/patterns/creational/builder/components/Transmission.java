package ru.nlmk.study.patterns.creational.builder.components;

public enum Transmission {
    AUTO, MANUAL;
}
