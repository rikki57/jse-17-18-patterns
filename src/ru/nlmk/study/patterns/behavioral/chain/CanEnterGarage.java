package ru.nlmk.study.patterns.behavioral.chain;

public class CanEnterGarage extends Chain {
    private Garage garage;

    public CanEnterGarage(Garage garage) {
        this.garage = garage;
    }

    @Override
    public boolean check(Bike bike) {
        if (garage.buzy < garage.places){
            return checkNext(bike);
        }
        return false;
    }
}
