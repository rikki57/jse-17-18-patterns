package ru.nlmk.study.patterns.creational.afactory.car;

public class RaceCar implements Car{
    @Override
    public void wash() {
        System.out.println("Wash race car");
    }

    @Override
    public void drive() {
        System.out.println("Drice race car");
    }
}
