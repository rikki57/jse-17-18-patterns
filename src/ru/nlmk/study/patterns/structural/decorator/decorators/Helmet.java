package ru.nlmk.study.patterns.structural.decorator.decorators;

import ru.nlmk.study.patterns.structural.decorator.Bike;
import ru.nlmk.study.patterns.structural.decorator.Decorator;

public class Helmet extends Decorator {
    private Bike bike;

    public Helmet(Bike bike) {
        this.bike = bike;
    }

    @Override
    public String getDescription() {
        return bike.getDescription().concat(", with helmet");
    }

    @Override
    public double cost() {
        return bike.cost() + 80;
    }
}
