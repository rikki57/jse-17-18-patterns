package ru.nlmk.study.patterns.behavioral.observer;

public class Racer implements Subscriber{
    private String name;

    public Racer(String name) {
        this.name = name;
    }

    @Override
    public void update(Race race) {
        System.out.println("Wow! Racer ".concat(name).concat(" is going to race in ").concat(race.getInfo()));
    }
}
