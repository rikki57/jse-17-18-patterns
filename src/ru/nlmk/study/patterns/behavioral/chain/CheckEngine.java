package ru.nlmk.study.patterns.behavioral.chain;

public class CheckEngine extends Chain{
    @Override
    public boolean check(Bike bike) {
        if (bike.isEngineRunning()){
            return checkNext(bike);
        }
        return false;
    }
}
