package ru.nlmk.study.patterns.creational.factory;

public class PizzaFactory {
    Pizza createPizza(String pizzaType){
        switch (pizzaType) {
            case "pepperoni":
                return new PizzaPepperoni(true);
            case "margarita":
                return new PizzaMargarita(false);
        }
        return null;
    }
}
