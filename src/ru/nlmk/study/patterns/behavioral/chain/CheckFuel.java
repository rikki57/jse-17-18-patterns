package ru.nlmk.study.patterns.behavioral.chain;

public class CheckFuel extends Chain {
    @Override
    public boolean check(Bike bike) {
        if (bike.getFuelLevel() > 0){
            return checkNext(bike);
        }
        return false;
    }
}
