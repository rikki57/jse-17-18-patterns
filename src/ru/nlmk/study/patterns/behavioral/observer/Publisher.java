package ru.nlmk.study.patterns.behavioral.observer;

public interface Publisher {
    void addRacer(Racer racer);
    void deleteRacer(Racer racer);
    void notifyRacers(Race race);
}
