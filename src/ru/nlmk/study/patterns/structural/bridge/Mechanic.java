package ru.nlmk.study.patterns.structural.bridge;

public class Mechanic implements Handler {
    @Override
    public void handle() {
        System.out.println("repairing transport");
    }
}
