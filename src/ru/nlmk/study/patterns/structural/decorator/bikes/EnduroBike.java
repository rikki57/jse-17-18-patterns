package ru.nlmk.study.patterns.structural.decorator.bikes;

import ru.nlmk.study.patterns.structural.decorator.Bike;

public class EnduroBike extends Bike {
    public EnduroBike() {
        description = "Enduro Bike";
    }

    @Override
    public double cost() {
        return 5000;
    }
}
