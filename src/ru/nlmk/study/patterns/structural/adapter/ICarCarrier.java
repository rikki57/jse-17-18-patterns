package ru.nlmk.study.patterns.structural.adapter;

public interface ICarCarrier {
    boolean transportCar(Car car);
}
