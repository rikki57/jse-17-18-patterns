package ru.nlmk.study.patterns.creational.builder.builder;

import ru.nlmk.study.patterns.creational.builder.Bike;
import ru.nlmk.study.patterns.creational.builder.components.Engine;
import ru.nlmk.study.patterns.creational.builder.components.Transmission;
import ru.nlmk.study.patterns.creational.builder.components.TripComputer;

public interface Builder {
    void setEngine(Engine engine);
    void setTransmission(Transmission transmission);
    void setFuel(double fuel);
    void setSeats(int seats);
    void setComputer(TripComputer computer);
    Bike getResult();
}
