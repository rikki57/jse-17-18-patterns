package ru.nlmk.study.patterns.creational.afactory.car;

public class OffroadCar implements Car{
    @Override
    public void wash() {
        System.out.println("Wash offroad car");
    }

    @Override
    public void drive() {
        System.out.println("Drice offroad car");
    }
}
