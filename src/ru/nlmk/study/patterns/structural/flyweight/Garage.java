package ru.nlmk.study.patterns.structural.flyweight;

import java.util.ArrayList;
import java.util.List;

public class Garage {
    private List<Bike> bikes = new ArrayList<>();

    public void makeBike(int stateReg, String modelName, String color){
        BikeType type = BikeFactory.getBikeType(modelName, color);
        Bike bike = new Bike(stateReg, type);
        bikes.add(bike);
    }
}
