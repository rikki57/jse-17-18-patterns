package ru.nlmk.study.patterns.behavioral.observer;

public class Test {
    public static void main(String[] args) {
        Publisher raceInformator = new PublisherImpl();

        Racer racer = new Racer("Gilles Villeneuve");
        raceInformator.addRacer(racer);
        racer = new Racer("Ayrton Senna");
        raceInformator.addRacer(racer);
        racer = new Racer("Bruce Mclaren");
        raceInformator.addRacer(racer);

        raceInformator.notifyRacers(new Race("Monza, 20th of may"));
        raceInformator.deleteRacer(racer);
        raceInformator.notifyRacers(new Race("Spa, 27th of may"));

    }
}
