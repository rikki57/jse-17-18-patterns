package ru.nlmk.study.patterns.fundamental.immutable;

public class Group {
    private Integer groupNumber;
    private String name;

    public Group(Integer groupNumber, String name) {
        this.groupNumber = groupNumber;
        this.name = name;
    }

    public Integer getGroupNumber() {
        return groupNumber;
    }


    public String getName() {
        return name;
    }

    public void setGroupNumber(Integer groupNumber) {
        this.groupNumber = groupNumber;
    }

    public void setName(String name) {
        this.name = name;
    }
}
