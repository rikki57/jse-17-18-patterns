package ru.nlmk.study.patterns.behavioral.chain;

public class Bike {
    private boolean engineRunning = false;
    private int fuelLevel = 0;

    public Bike(boolean engineRunning, int fuelLevel) {
        this.engineRunning = engineRunning;
        this.fuelLevel = fuelLevel;
    }

    public boolean isEngineRunning() {
        return engineRunning;
    }

    public void setEngineRunning(boolean engineRunning) {
        this.engineRunning = engineRunning;
    }

    public int getFuelLevel() {
        return fuelLevel;
    }

    public void setFuelLevel(int fuelLevel) {
        this.fuelLevel = fuelLevel;
    }
}
