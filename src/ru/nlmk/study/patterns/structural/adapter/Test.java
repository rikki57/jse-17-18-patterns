package ru.nlmk.study.patterns.structural.adapter;

public class Test {
    public static void main(String[] args) {
        CarCarrier carCarrier = new CarCarrier(5);
        Car car = new Car();
        car.setWeight(2);
        if (carCarrier.transportCar(car)){
            System.out.println("Success!");
        } else {
            System.out.println("Not success");
        }

        System.out.println("");
        Bike bike = new Bike("1");
        BikeAdapter adapter = new BikeAdapter(bike);
        if (carCarrier.transportCar(adapter)){
            System.out.println("Success!");
        } else {
            System.out.println("Not success");
        }
    }
}
