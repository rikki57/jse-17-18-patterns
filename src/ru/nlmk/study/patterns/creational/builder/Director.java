package ru.nlmk.study.patterns.creational.builder;

import ru.nlmk.study.patterns.creational.builder.builder.Builder;
import ru.nlmk.study.patterns.creational.builder.components.Engine;
import ru.nlmk.study.patterns.creational.builder.components.Transmission;
import ru.nlmk.study.patterns.creational.builder.components.TripComputer;

public class Director {
    public void constructSportBike(Builder builder){
        builder.setEngine(new Engine(true, 100, 0));
        builder.setFuel(100);
        builder.setSeats(1);
        builder.setTransmission(Transmission.MANUAL);
    }

    public void constructChopperBike(Builder builder){
        builder.setEngine(new Engine(false, 50, 1000));
        builder.setFuel(200);
        builder.setSeats(2);
        builder.setTransmission(Transmission.MANUAL);
        builder.setComputer(new TripComputer());
    }
}
