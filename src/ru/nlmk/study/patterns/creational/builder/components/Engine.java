package ru.nlmk.study.patterns.creational.builder.components;

public class Engine {
    private boolean started;
    private double volume;
    private double mileage;

    public Engine(boolean started, double volume, double mileage) {
        this.started = started;
        this.volume = volume;
        this.mileage = mileage;
    }

    public boolean isStarted() {
        return started;
    }

    public void setStarted(boolean started) {
        this.started = started;
    }

    public double getVolume() {
        return volume;
    }

    public void setVolume(double volume) {
        this.volume = volume;
    }

    public double getMileage() {
        return mileage;
    }

    public void setMileage(double mileage) {
        this.mileage = mileage;
    }

    public void go(double mileage){
        if (started){
            this.mileage += mileage;
        }
    }
}
