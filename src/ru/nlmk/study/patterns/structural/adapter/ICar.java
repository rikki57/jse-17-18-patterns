package ru.nlmk.study.patterns.structural.adapter;

public interface ICar {
    void driveTo(String destination);
}
