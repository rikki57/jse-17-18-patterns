package ru.nlmk.study.patterns.creational.factory;

public class PizzaPepperoni extends Pizza {
    public PizzaPepperoni(boolean thick) {
        super("cheese2", thick);
    }
}
