package ru.nlmk.study.patterns.structural.adapter;

public class Bike {
    private String weight;

    public Bike(String weight) {
        this.weight = weight;
    }

    public String getWeight() {
        return weight;
    }

    public void raceTo(String destination, int speed){
        System.out.println("Bike rides, target: ".concat(destination).concat(", speed: ".concat(String.valueOf(speed))));
    }
}
