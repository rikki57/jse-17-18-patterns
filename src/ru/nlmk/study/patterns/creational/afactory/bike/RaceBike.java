package ru.nlmk.study.patterns.creational.afactory.bike;

public class RaceBike implements Bike{
    @Override
    public void repair() {
        System.out.println("Repair race bike");
    }

    @Override
    public void ride() {
        System.out.println("Rike race bike");
    }
}
