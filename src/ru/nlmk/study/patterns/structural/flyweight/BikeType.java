package ru.nlmk.study.patterns.structural.flyweight;

public class BikeType {
    private String modelName;
    private String color;

    public BikeType(String modelName, String color) {
        this.modelName = modelName;
        this.color = color;
    }

    public void print(int stateReg){
        System.out.println(stateReg + " " + modelName + " " + color);
    }
}
