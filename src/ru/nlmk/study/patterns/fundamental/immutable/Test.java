package ru.nlmk.study.patterns.fundamental.immutable;

public class Test {
    public static void main(String[] args) {
        Group group = new Group(1, "name");
        User user = new User("Alex", group);
        group.setGroupNumber(2);
        System.out.println(user.getGroup().getGroupNumber());
    }
}
