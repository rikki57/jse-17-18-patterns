package ru.nlmk.study.patterns.structural.decorator.bikes;

import ru.nlmk.study.patterns.structural.decorator.Bike;

public class SportBike extends Bike {
    public SportBike() {
        description = "Sport Bike";
    }

    @Override
    public double cost() {
        return 7000;
    }
}