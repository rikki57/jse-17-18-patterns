package ru.nlmk.study.patterns.fundamental.immutable;

public class User {
    private final String name;
    private final Group group;

    public User(String name, Group group) {
        this.name = name;
        this.group = new Group(group.getGroupNumber(), group.getName());
    }

    public String getName() {
        return name;
    }

    public Group getGroup() {
        return new Group(group.getGroupNumber(), group.getName());
    }
}
