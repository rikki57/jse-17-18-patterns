package ru.nlmk.study.patterns.behavioral.state;

public class OffState extends State {
    public OffState(Bike bike) {
        super(bike);
    }

    @Override
    public void onStart() {
        bike.setEngineRunning(true);
        bike.setState(new OnState(bike));
        System.out.println("Success-engine started");
    }

    @Override
    public void onRide() {
        System.out.println("error");
    }

    @Override
    public void onArrive() {
        System.out.println("error");
    }

    @Override
    public void onOff() {
        System.out.println("error");
    }
}
