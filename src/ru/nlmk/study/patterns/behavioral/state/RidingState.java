package ru.nlmk.study.patterns.behavioral.state;

public class RidingState extends State{
    public RidingState(Bike bike) {
        super(bike);
    }

    @Override
    public void onStart() {
        System.out.println("error");
    }

    @Override
    public void onRide() {
        System.out.println("error");
    }

    @Override
    public void onArrive() {
        bike.setRiding(false);
        bike.setState(new ArrivedState(bike));
        System.out.println("Success");
    }

    @Override
    public void onOff() {
        System.out.println("error");
    }
}
