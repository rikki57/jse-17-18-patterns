package ru.nlmk.study.patterns.fundamental.minterface;

public class MyCloneable implements Cloneable{
    private Integer val1;
    private Integer val2;

    public MyCloneable(Integer val1, Integer val2) {
        this.val1 = val1;
        this.val2 = val2;
    }

    public Integer getVal1() {
        return val1;
    }

    public void setVal1(Integer val1) {
        this.val1 = val1;
    }

    public Integer getVal2() {
        return val2;
    }

    public void setVal2(Integer val2) {
        this.val2 = val2;
    }

    @Override
    protected MyCloneable clone() {
        try{
            super.clone();
        } catch (CloneNotSupportedException e){
            e.printStackTrace();
        }
        return new MyCloneable(val1, val2);
    }

    @Override
    public String toString() {
        return "MyCloneable{" +
                "val1=" + val1 +
                ", val2=" + val2 +
                '}';
    }
}
