package ru.nlmk.study.patterns.behavioral.state;

public class OnState extends State {
    public OnState(Bike bike) {
        super(bike);
    }

    @Override
    public void onStart() {
        System.out.println("error");
    }

    @Override
    public void onRide() {
        bike.setRiding(true);
        bike.setState(new RidingState(bike));
        System.out.println("Success");
    }

    @Override
    public void onArrive() {
        System.out.println("error");
    }

    @Override
    public void onOff() {
        bike.setEngineRunning(false);
        bike.setState(new OnState(bike));
        System.out.println("success");
    }
}
