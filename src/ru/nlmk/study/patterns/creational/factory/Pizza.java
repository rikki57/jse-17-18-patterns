package ru.nlmk.study.patterns.creational.factory;

public abstract class Pizza {
    private String cheese;
    private boolean thick;

    @Override
    public String toString() {
        return "Pizza{" +
                "cheese='" + cheese + '\'' +
                ", thick=" + thick +
                '}';
    }

    public Pizza(String cheese, boolean thick) {
        this.cheese = cheese;
        this.thick = thick;
    }

    public String getCheese() {
        return cheese;
    }

    public void setCheese(String cheese) {
        this.cheese = cheese;
    }

    public boolean isThick() {
        return thick;
    }

    public void setThick(boolean thick) {
        this.thick = thick;
    }
}
