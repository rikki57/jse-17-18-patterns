package ru.nlmk.study.patterns.structural.flyweight;

public class Test {
    static int BIKES_TO_MAKE = 1_000_000;
    static String model1 = "Suzuki";
    static String model2 = "Honda";
    static String model3 = "Ducatti";

    public static void main(String[] args) {
        Garage garage = new Garage();
        for (int i = 0; i < Math.floor(BIKES_TO_MAKE / 9); i++){
            garage.makeBike(random(1_000_000, 100_000_000), model1, "Blue");
            garage.makeBike(random(1_000_000, 100_000_000), model1, "Black");
            garage.makeBike(random(1_000_000, 100_000_000), model1, "Yellow");
            garage.makeBike(random(1_000_000, 100_000_000), model2, "Red");
            garage.makeBike(random(1_000_000, 100_000_000), model2, "Grey");
            garage.makeBike(random(1_000_000, 100_000_000), model2, "Green");
            garage.makeBike(random(1_000_000, 100_000_000), model3, "Pink");
            garage.makeBike(random(1_000_000, 100_000_000), model3, "Dark-Grey");
            garage.makeBike(random(1_000_000, 100_000_000), model3, "Dark-Green");
        }
        System.out.println(BikeFactory.bikeTypes.size());
    }

    private static int random(int min, int max){
        return min + (int)(Math.random() * ((max-min) + 1));
    }
}
