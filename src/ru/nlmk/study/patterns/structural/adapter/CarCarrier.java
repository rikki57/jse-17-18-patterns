package ru.nlmk.study.patterns.structural.adapter;

public class CarCarrier implements ICarCarrier {
    private double carWeight;

    public CarCarrier(double carWeight) {
        this.carWeight = carWeight;
    }

    public double getCarWeight() {
        return carWeight;
    }

    @Override
    public boolean transportCar(Car car) {
        boolean result = false;
        if (carWeight < car.getWeight()){
            return false;
        }
        car.driveTo("ЗАехать на перевозчик");
        System.out.println("Перевозим машину");
        car.driveTo("съехать с перевозчика");
        return true;
    }
}
