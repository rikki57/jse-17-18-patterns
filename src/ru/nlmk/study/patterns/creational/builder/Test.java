package ru.nlmk.study.patterns.creational.builder;

import ru.nlmk.study.patterns.creational.builder.builder.BikeBuilder;
import ru.nlmk.study.patterns.creational.builder.components.TripComputer;

public class Test {
    public static void main(String[] args) {
        //Director director = new Director();
        BikeBuilder builder= new BikeBuilder();
        //director.constructSportBike(builder);
        builder.setComputer(new TripComputer());
        builder.setFuel(100);
        builder.getResult();
        Bike bike = builder.getResult();
        //director.constructChopperBike(builder);
        //Bike bike2 = builder.getResult();
        System.out.println("!!1");
    }
}
