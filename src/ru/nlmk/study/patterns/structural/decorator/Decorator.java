package ru.nlmk.study.patterns.structural.decorator;

public abstract class Decorator extends Bike{
    public abstract String getDescription();
}
