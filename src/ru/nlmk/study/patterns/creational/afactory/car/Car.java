package ru.nlmk.study.patterns.creational.afactory.car;

public interface Car {
    void wash();
    void drive();
}
