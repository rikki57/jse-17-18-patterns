package ru.nlmk.study.patterns.fundamental.minterface;

public class Test {
    public static void main(String[] args) {
        MyCloneable v1 = new MyCloneable(1, 2);
        MyCloneable v2 = v1.clone();
        System.out.println(v2);
    }
}
