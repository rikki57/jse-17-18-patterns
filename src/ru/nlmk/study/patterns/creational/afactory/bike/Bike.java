package ru.nlmk.study.patterns.creational.afactory.bike;

public interface Bike {
    void repair();
    void ride();
}
