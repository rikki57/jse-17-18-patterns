package ru.nlmk.study.patterns.behavioral.chain;

public abstract class Chain {
    private Chain next;

    public Chain linkWith(Chain next){
        this.next = next;
        return next;
    }

    public abstract boolean check(Bike bike);

    protected boolean checkNext(Bike bike){
        if (next == null){
            return true;
        }
        return next.check(bike);
    }
}
