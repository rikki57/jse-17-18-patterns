package ru.nlmk.study.patterns.structural.decorator.bikes;

import ru.nlmk.study.patterns.structural.decorator.Bike;

public class PitBike  extends Bike {
    public PitBike() {
        description = "Pit Bike";
    }

    @Override
    public double cost() {
        return 4000;
    }
}
