package ru.nlmk.study.patterns.behavioral.chain;

import ru.nlmk.study.patterns.fundamental.immutable.Group;

public class Test {
    public static void main(String[] args) {
        Bike bike = new Bike(true, 20);
        Garage garage = new Garage(6, 4);

        Chain garageEnterChain = new CanEnterGarage(garage);
        garageEnterChain.linkWith(new CheckEngine()).linkWith(new CheckFuel());

        System.out.println(garageEnterChain.check(bike));
    }
}
