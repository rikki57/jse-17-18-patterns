package ru.nlmk.study.patterns.creational.afactory.factory;

import ru.nlmk.study.patterns.creational.afactory.bike.Bike;
import ru.nlmk.study.patterns.creational.afactory.bike.OffroadBike;
import ru.nlmk.study.patterns.creational.afactory.car.Car;
import ru.nlmk.study.patterns.creational.afactory.car.OffroadCar;

public class OffroadFactory extends AbstractFactory{
    @Override
    public Car createCar() {
        return new OffroadCar();
    }

    @Override
    public Bike createBike() {
        return new OffroadBike();
    }
}
