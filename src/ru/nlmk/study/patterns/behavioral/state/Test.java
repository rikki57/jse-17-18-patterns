package ru.nlmk.study.patterns.behavioral.state;

public class Test {
    public static void main(String[] args) {
        Bike bike = new Bike();
        bike.getState().onStart();
        bike.getState().onRide();
        bike.getState().onOff();
        bike.getState().onArrive();
        bike.getState().onOff();
    }
}
