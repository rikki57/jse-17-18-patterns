package ru.nlmk.study.patterns.creational.builder.builder;

import ru.nlmk.study.patterns.creational.builder.Bike;
import ru.nlmk.study.patterns.creational.builder.components.Engine;
import ru.nlmk.study.patterns.creational.builder.components.Transmission;
import ru.nlmk.study.patterns.creational.builder.components.TripComputer;

public class BikeBuilder implements Builder{
    private double fuel = 0;
    private int seats;
    private Engine engine;
    private Transmission transmission;
    private TripComputer computer;

    @Override
    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    @Override
    public void setTransmission(Transmission transmission) {
        this.transmission = transmission;
    }

    @Override
    public void setFuel(double fuel) {
        this.fuel = fuel;
    }

    @Override
    public void setSeats(int seats) {
        this.seats = seats;
    }

    @Override
    public void setComputer(TripComputer computer) {
        this.computer = computer;
    }

    @Override
    public Bike getResult() {
        return new Bike(fuel, seats, engine, transmission, computer);
    }
}
