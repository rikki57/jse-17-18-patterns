package ru.nlmk.study.patterns.behavioral.observer;

public interface Subscriber {
    void update(Race race);
}
