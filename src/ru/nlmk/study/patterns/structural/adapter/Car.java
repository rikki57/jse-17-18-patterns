package ru.nlmk.study.patterns.structural.adapter;

public class Car implements ICar{
    private double weight;

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getWeight() {
        return weight;
    }

    @Override
    public void driveTo(String destination) {
        System.out.println("Едем, цель: ".concat(destination));
    }
}
