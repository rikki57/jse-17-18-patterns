package ru.nlmk.study.patterns.structural.bridge;

public class Car extends Transport {
    public Car(Handler handler) {
        super(handler);
    }

    @Override
    public void workWith() {
        System.out.println("Working with car in progress");
        handler.handle();
    }
}
