package ru.nlmk.study.patterns.structural.decorator;

import ru.nlmk.study.patterns.structural.decorator.bikes.SportBike;
import ru.nlmk.study.patterns.structural.decorator.decorators.Helmet;
import ru.nlmk.study.patterns.structural.decorator.decorators.RaceTyres;

public class Test {
    public static void main(String[] args) {
        Bike bike = new SportBike();
        bike = new Helmet(new RaceTyres(new RaceTyres(bike)));

        System.out.println(bike.getDescription());
        System.out.println(bike.cost());
    }
}
