package ru.nlmk.study.patterns.creational.builder;

import ru.nlmk.study.patterns.creational.builder.components.Engine;
import ru.nlmk.study.patterns.creational.builder.components.Transmission;
import ru.nlmk.study.patterns.creational.builder.components.TripComputer;

public class Bike {
    private double fuel = 0;
    private int seats;
    private Engine engine;
    private Transmission transmission;
    private TripComputer computer;

    public Bike(double fuel, int seats, Engine engine, Transmission transmission, TripComputer computer) {
        this.fuel = fuel;
        this.seats = seats;
        this.engine = engine;
        this.transmission = transmission;
        this.computer = computer;
    }

    public double getFuel() {
        return fuel;
    }

    public void setFuel(double fuel) {
        this.fuel = fuel;
    }

    public int getSeats() {
        return seats;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }

    public Engine getEngine() {
        return engine;
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    public Transmission getTransmission() {
        return transmission;
    }

    public void setTransmission(Transmission transmission) {
        this.transmission = transmission;
    }

    public TripComputer getComputer() {
        return computer;
    }

    public void setComputer(TripComputer computer) {
        this.computer = computer;
    }
}
