package ru.nlmk.study.patterns.behavioral.command;

public class AccidentFlash extends Command{
    public AccidentFlash(Bike bike) {
        super(bike);
    }

    @Override
    public Boolean execute() {
        allOff();
        bike.setRightFlash(true);
        bike.setLeftFlash(true);
        return true;
    }
}
