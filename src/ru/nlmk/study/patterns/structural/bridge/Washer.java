package ru.nlmk.study.patterns.structural.bridge;

public class Washer implements Handler {
    @Override
    public void handle() {
        System.out.println("I'm washing transport");
    }
}
