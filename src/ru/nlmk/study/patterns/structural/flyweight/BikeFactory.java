package ru.nlmk.study.patterns.structural.flyweight;

import java.util.HashMap;
import java.util.Map;

public class BikeFactory {
    static Map<String, BikeType> bikeTypes = new HashMap<>();

    public static BikeType getBikeType(String modelName, String color){
        BikeType result = bikeTypes.get(modelName + color);
        if (result == null){
            result = new BikeType(modelName, color);
            bikeTypes.put(modelName + color, result);
        }
        return result;
    }
}
