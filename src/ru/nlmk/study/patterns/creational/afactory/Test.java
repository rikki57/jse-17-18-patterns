package ru.nlmk.study.patterns.creational.afactory;

import ru.nlmk.study.patterns.creational.afactory.factory.AbstractFactory;
import ru.nlmk.study.patterns.creational.afactory.factory.OffroadFactory;
import ru.nlmk.study.patterns.creational.afactory.factory.RaceFactory;

public class Test {
    public static void main(String[] args) {
        Driver driver = null;
        AbstractFactory abstractFactory;
        String condition = "race";
        switch (condition){
            case "offroad":
                abstractFactory = new OffroadFactory();
                driver = new Driver(abstractFactory);
                driver.use();
                break;
            case "race":
                abstractFactory = new RaceFactory();
                driver = new Driver(abstractFactory);
                driver.use();
                break;
        }
    }
}
