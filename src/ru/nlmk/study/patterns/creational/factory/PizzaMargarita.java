package ru.nlmk.study.patterns.creational.factory;

public class PizzaMargarita extends Pizza {
    public PizzaMargarita(boolean thick) {
        super("Cheese1", thick);
    }
}
