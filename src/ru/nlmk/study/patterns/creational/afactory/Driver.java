package ru.nlmk.study.patterns.creational.afactory;

import ru.nlmk.study.patterns.creational.afactory.bike.Bike;
import ru.nlmk.study.patterns.creational.afactory.car.Car;
import ru.nlmk.study.patterns.creational.afactory.factory.AbstractFactory;

public class Driver {
    private Car car;
    private Bike bike;

    public Driver(AbstractFactory abstractFactory) {
        this.car = abstractFactory.createCar();
        this.bike = abstractFactory.createBike();
    }

    public void use(){
        car.drive();
        car.wash();
        bike.repair();
        bike.ride();
    }
}
