package ru.nlmk.study.patterns.creational.singletone;

public class Singletone {
    private Singletone() {
    }

    private static Singletone instance = null;

    public static Singletone getInstance(){
        if (instance == null){
            instance = new Singletone();
        }
        return instance;
    }

    public void doWork(){
        System.out.println("Working");
    }
}
