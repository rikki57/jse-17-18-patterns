package ru.nlmk.study.patterns.structural.decorator;

public abstract class Bike {
    protected String description;

    public String getDescription() {
        return description;
    }

    public abstract double cost();
}
