package ru.nlmk.study.patterns.behavioral.state;

public class ArrivedState extends State {
    public ArrivedState(Bike bike) {
        super(bike);
    }

    @Override
    public void onStart() {
        System.out.println("error");
    }

    @Override
    public void onRide() {
        bike.setRiding(true);
        bike.setState(new RidingState(bike));
        System.out.println("Success");
    }

    @Override
    public void onArrive() {
        System.out.println("error");
    }

    @Override
    public void onOff() {
        bike.setEngineRunning(false);
        bike.setState(new OffState(bike));
        System.out.println("Success");
    }
}
