package ru.nlmk.study.patterns.structural.bridge;

public class Bike extends Transport {
    public Bike(Handler handler) {
        super(handler);
    }

    @Override
    public void workWith() {
        System.out.println("Working with bike in progress");
        handler.handle();
    }
}
