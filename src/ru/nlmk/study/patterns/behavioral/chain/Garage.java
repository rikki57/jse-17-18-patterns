package ru.nlmk.study.patterns.behavioral.chain;

public class Garage {
    int places;
    int buzy;

    public Garage(int places, int buzy) {
        this.places = places;
        this.buzy = buzy;
    }

    public int getPlaces() {
        return places;
    }

    public void setPlaces(int places) {
        this.places = places;
    }

    public int getBuzy() {
        return buzy;
    }

    public void setBuzy(int buzy) {
        this.buzy = buzy;
    }
}
