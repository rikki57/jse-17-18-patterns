package ru.nlmk.study.patterns.creational.afactory.factory;

import ru.nlmk.study.patterns.creational.afactory.bike.Bike;
import ru.nlmk.study.patterns.creational.afactory.bike.RaceBike;
import ru.nlmk.study.patterns.creational.afactory.car.Car;
import ru.nlmk.study.patterns.creational.afactory.car.RaceCar;

public class RaceFactory extends AbstractFactory{
    @Override
    public Car createCar() {
        return new RaceCar();
    }

    @Override
    public Bike createBike() {
        return new RaceBike();
    }
}
