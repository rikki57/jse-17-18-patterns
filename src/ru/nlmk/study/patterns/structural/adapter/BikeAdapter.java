package ru.nlmk.study.patterns.structural.adapter;

public class BikeAdapter extends Car{
    private Bike bike;

    public BikeAdapter(Bike bike) {
        this.bike = bike;
    }

    @Override
    public void driveTo(String destination) {
        bike.raceTo(destination, 5);
    }
}
