package ru.nlmk.study.patterns.structural.bridge;

public interface Handler {
    void handle();
}
