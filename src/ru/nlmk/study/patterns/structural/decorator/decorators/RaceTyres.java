package ru.nlmk.study.patterns.structural.decorator.decorators;

import ru.nlmk.study.patterns.structural.decorator.Bike;
import ru.nlmk.study.patterns.structural.decorator.Decorator;

public class RaceTyres extends Decorator {
    private Bike bike;

    public RaceTyres(Bike bike) {
        this.bike = bike;
    }

    @Override
    public String getDescription() {
        return bike.getDescription().concat(", with race tyres");
    }

    @Override
    public double cost() {
        return bike.cost() + 120;
    }
}
