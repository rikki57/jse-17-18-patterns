package ru.nlmk.study.patterns.creational.afactory.bike;

public class OffroadBike implements Bike{
    @Override
    public void repair() {
        System.out.println("Repair offroad bike");
    }

    @Override
    public void ride() {
        System.out.println("Rike offroad bike");
    }
}
